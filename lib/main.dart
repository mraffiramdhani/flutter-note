import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Quick Note App",
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 60, left: 40, right: 40),
            child: Container(
              child: Row(
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Image.asset(
                          'images/gura.jpg',
                          width: 200,
                          height: 200,
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [Text('Bruh')],
                  ),
                ],
              ),
            ),
          ),
        ),
        backgroundColor: Colors.white);
  }
}
